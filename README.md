# rstools

My personal tools for Cargo projects.

## Compile

The `compile` script allows to update the subversion specified within 
the  `Cargo.toml` before running the `cargo build` command.

In the future developments would be added some more features.
